<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username')->unique();
			$table->string('email_address')->unique();
			$table->string('password');

			$table->string('first_name');
			$table->string('last_name');

			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});

		Schema::create('games', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('user_name');
			$table->string('name');
			$table->string('description');
			$table->integer('num_teams');   //For loop in form
		});

		Schema::create('game_info', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('game_id');
			$table->string('name');
			$table->string('info');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
		Schema::dropIfExists('games');
		Schema::dropIfExists('game_info');
	}

}
