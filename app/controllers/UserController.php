<?php

class UserController extends BaseController {


  public function index()
  {

    $user_id = Auth::id();

    $username = DB::table('users')
      ->where('id', $user_id)
      ->pluck('username');

    $email = DB::table('users')
      ->where('id', $user_id)
      ->pluck('email_address');

    return View::make('settings', array(
      'username' => $username,
      'email' => $email
    ));
  }

  public function changeUsername(){
    $input = Input::all();

    $user_id = Auth::id();

    $validator = Validator::make(
        array(
            'username'        => $input['username'],
        ),
        array(
            'username'        => 'required|min:2|unique:users,email_address',
        )
    );

    $messages = $validator->messages();
    if(count($messages) > 0){
      Session::flash('errors',  $messages->all());
      return Redirect::back();
    }


    // TODO: actually use the user class for this

    DB::table('users')
      ->where('id', $user_id)
      ->update(array(
        'username' => $input['username']
    ));

    // redirect to dashboard
    Session::flash('user-status', 'Your username has been changed.');
    return Redirect::back();
  }


  public function changeEmail(){
    $input = Input::all();

    $user_id = Auth::id();

    $validator = Validator::make(
        array(
            'email'        => $input['email'],
        ),
        array(
            'email'        => 'required|min:2|unique:users,email_address|email',
        )
    );

    $messages = $validator->messages();
    if(count($messages) > 0){
      Session::flash('errors',  $messages->all());
      return Redirect::back();
    }


    // TODO: actually use the user class for this

    DB::table('users')
      ->where('id', $user_id)
      ->update(array(
        'email_address' => $input['email']
    ));

    // redirect to dashboard
    Session::flash('user-status', 'Your email address has been changed.');
    return Redirect::back();
  }

  public function changePassword(){
    $input = Input::all();

    $validator = Validator::make(
        array(
            'new_password'            => $input['passNew'],
            'confirm_password'        => $input['passConfirm'],
        ),
        array(
            'new_password'            => 'required|min:6',
            'confirm_password'        => 'required|min:6|same:new_password',
        )
    );

    $messages = $validator->messages();
    if(count($messages) > 0){
      Session::flash('errors',  $messages->all());
      return Redirect::back();
    }


    $user_id = Auth::id();

    $password = DB::table('users')
      ->where('id', $user_id)
      ->pluck('password');


    if (Hash::check($input['old_password'], $password))
    {

          DB::table('users')
            ->where('id', $user_id)
            ->update(array(
              'password' => Hash::make($input['passNew'])

          ));

          Session::flash('user-status',  'Password updated!');
          return Redirect::back();
    }
    else{
      Session::flash('error',  'Please enter your current password');
      return Redirect::back();
    }
  }

  public function deleteUser(){
    $input = Input::all();

    $user_id = Auth::id();

    $password = DB::table('users')
      ->where('id', $user_id)
      ->pluck('password');

    if (Hash::check($input['password'], $password))
    {

          $username = DB::table('users')
            ->where('id', $user_id)
            ->pluck('username');

          $sheets = DB::table('sheets')
            ->where('user_name', $username)
            ->select('id')
            ->get();

          foreach($sheets as $sheet){
            DB::table('sheet_info')
              ->where('sheet_id', $sheet->id)
              ->delete();
          }

          DB::table('sheets')
            ->where('user_name', $username)
            ->delete();

          DB::table('users')
            ->where('id', $user_id)
            ->delete();

          Session::flash('user-deleted',  'Your account has been deleted.');
          return Redirect::to('/login');
    }
    else{
      Session::flash('error',  'Incorrect password');
      return Redirect::back();
    }
  }
}
