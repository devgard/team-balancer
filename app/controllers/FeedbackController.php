<?php

class FeedbackController extends BaseController {

  public function create(){

    $data = Input::all();


    Mail::send('emails.feedback', $data, function($message)
    {
      $message->from('noreply@amtgard.io', ' Team balancer tool');
      $message->to('jfefes@gmail.com')->subject('New feedback reported!');
    });

    return Redirect::back();
  }

}
